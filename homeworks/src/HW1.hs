module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 0 = 0
hw1_2 n = hw1_2 (n-1) + 1.0/fromIntegral(n^n)

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 0 = 1
fact2 1 = 1
fact2 n = n*fact2(n-2)

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
primeNums = 2 : [n | n <- [3..], isPrime n]
isPrime n = foldr (\p r-> p*p>n || (rem n p /= 0 && r)) True primeNums

-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b = if (a < b) then if isPrime(a) 
						then a + primeSum (a + 1) b 
						else primeSum (a + 1) b
							else if isPrime(b) 
							then b 
							else 0

