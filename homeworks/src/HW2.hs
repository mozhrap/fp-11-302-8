-- Тесты чуть позже

module HW2
        ( Contact (..)
        , isKnown
        , Term (..)
        , eval
        , simplify
        ) where

data Contact = On
              | Off
              | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval (Const a) = a
eval x = case x of
  Mult a b -> eval a * eval b
  Add  a b -> eval a + eval b
  Sub  a b -> eval a - eval b

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify x = case x of 
  (Const a) -> Const a
  (Mult a (Add b c)) -> simplify(Add (Mult (simplify a) (simplify b)) (Mult (simplify a) (simplify c)))
  (Mult (Add b c) a) -> simplify(Add (Mult (simplify b) (simplify a)) (Mult (simplify c) (simplify a)))   
  (Mult a (Sub b c)) -> simplify(Sub (Mult (simplify a) (simplify b)) (Mult (simplify a) (simplify c)))
  (Mult (Sub b c) a) -> simplify(Sub (Mult (simplify b) (simplify a)) (Mult (simplify c) (simplify a)))
  (Add a b) -> Add (simplify a) (simplify b)
  (Sub a b) -> Sub (simplify a) (simplify b)
  (Mult a b) -> Mult (simplify a) (simplify b)