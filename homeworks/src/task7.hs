module Task where

  import Data.List
  main :: IO ()
  main = print (start 0 0)
    -- общее определение (все натуральные числа > 1, которые являются простыми)
  primeNums = 2 : [n | n <- [3..], isPrime n]
  -- Число простое, если у него нет (простых) делителей
  isPrime n = foldr (\p r-> p*p>n || (rem n p /= 0 && r)) True primeNums

  start :: Int -> Int -> Int
  start a b =
    if isPrime a
        then 
          if b>10001
            then a
            else start (a+1) (b+1)
        else start (a+1) (b)